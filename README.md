# README #

### ¿Para qué es este repositorio? ###

* Este repositorio aloja un trabajo práctico realizado en Python para la materia Algoritmos y Programación II correspondiente a la Facultad de Ingeniería de la Universidad de Buenos Aires. El mismo consiste en un backend que, dados un mapa, fábricas dentro de éste y juguetes dentro de las fábricas, busca optimizar la cantidad de fábricas recorridas durante un día por un trineo navideño.
* Version 1.0

### Uso del programa ###

* Para correr el programa puede usarse cualquier intérprete de Python (en particular, yo lo corro por consola en linux)
* El programa responde a través de comandos automatizados, los mismos están descriptos en el enunciado provisto por la cátedra. El protocolo indica que se necesitan ciertos archivos csv: el de [mapa](http://i.imgur.com/uYEnr55.png), el de [fábricas](http://i.imgur.com/zvPGOB8.png) y el de [juguetes](http://i.imgur.com/ddj1Vyj.png).
* Se ha usado la librería numpy para representar las coordenadas y calcular la distancia entre ellas.
* Comparto las pruebas automatizadas provistas por la cátedra. Las instrucciones de corrida están en el enunciado.
* Un detalle: para la cursada, el opcional de interfaz gráfica no ha sido implementado, sin embargo para ver una representación gráfica de los resultados, puede hacerse uso del archivo kml que puede generarse en el programa. Para ello, suban el archivo kml en dropbox o similares y peguen el enlace del archivo luego del siguiente prefijo: http://maps.google.com/maps?q=

### Contacto ###

* Para cualquier información, consultar en https://bitbucket.org/lucasp90